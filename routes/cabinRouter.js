const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const authenticate = require('../authenticate');
const cors = require('./cors');
const Cabins = require('../models/cabins');

const cabinRouter = express.Router();

cabinRouter.use(bodyParser.json());


cabinRouter.route('/')
.options(cors.corsWithOptions, (req,res) => {res.sendStatus(200); })
.get(cors.cors, (req, res, next) => {
    Cabins.find(req.query)
    .populate('comments.author')
    .then((cabin) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(cabin);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.post(cors.corsWithOptions, /*authenticate.verifyUser, authenticate.verifyAdmin,*/ (req, res, next) => {
    Cabins.create(req.body)
    .then((cabin) => {
        console.log('Cabin Created', cabin);
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(cabin);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.put(cors.corsWithOptions, authenticate.verifyUser,authenticate.verifyAdmin, (req, res, next) => {
    res.statusCode = 403;
    res.end('PUT operation not supported on /cabins');
})
.delete(cors.corsWithOptions, /*authenticate.verifyUser, authenticate.verifyAdmin,*/ (req, res, next) => {
    Cabins.remove({})
    .then((resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    }, (err) => next(err))
    .catch((err) => next(err));
});

cabinRouter.route('/:cabinId')
.options(cors.corsWithOptions, (req,res) => {res.sendStatus(200); })
.get(cors.cors, (req, res, next) => {
    Cabins.findById(req.params.cabinId)
    .populate('comments.author')
    .then((cabin) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(cabin);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.post(cors.corsWithOptions,/*authenticate.verifyUser, authenticate.verifyAdmin,*/ (req, res, next) => {
    res.statusCode = 403;
    res.end('POST operation not supported on /cabins/' + req.params.cabinId);
})
.put(cors.corsWithOptions, /*authenticate.verifyUser, authenticate.verifyAdmin,*/ (req, res, next) => {
    Cabins.findByIdAndUpdate(req.params.cabinId, {
        $set: req.body
    }, {new: true})
    .then((cabin) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(cabin);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.delete(cors.corsWithOptions, /*authenticate.verifyUser, authenticate.verifyAdmin,*/ (req, res, next) => {
    Cabins.findByIdAndRemove(req.params.cabinId)
    .then((resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    }, (err) => next(err))
    .catch((err) => next(err));
});

cabinRouter.route('/:cabinId/comments')
.options(cors.corsWithOptions, (req,res) => {res.sendStatus(200); })
.get(cors.cors, (req, res, next) => {
    Cabins.findById(req.params.cabinId)
    .populate('comments.author')
    .then((cabin) => {
        if (cabin != null) {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json(cabin.comments);
        }
        else {
            err = new Error('Cabin ' + req.params.cabinId + ' not found');
            err.status = 404;
            return next(err);
        }
    }, (err) => next(err))
    .catch((err) => next(err));
})
.post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Cabins.findById(req.params.cabinId)
    .then((cabin) => {
        if (cabin != null) {
            req.body.author = req.user._id;
            cabin.comments.push(req.body);
            cabin.save()
            .then((cabin) => {
                Cabins.findById(cabin._id)
                .populate('comments.author')
                .then((cabin) => {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(cabin);
                })            
            }, (err) => next(err));
        }
        else {
            err = new Error('Cabin ' + req.params.cabinId + ' not found');
            err.status = 404;
            return next(err);
        }
    }, (err) => next(err))
    .catch((err) => next(err));
})
.put(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    res.statusCode = 403;
    res.end('PUT operation not supported on /cabins/' + req.params.cabinId + '/comments');
})
.delete(cors.corsWithOptions, authenticate.verifyUser,authenticate.verifyAdmin, (req, res, next) => {
    Cabins.findById(req.params.cabinId)
    .then((cabin) => {
        if (cabin != null || req.body.author._id) {
            for (var i = (cabin.comments.length -1); i >= 0; i--) {
                cabin.comments.id(cabin.comments[i]._id).remove();
            }
            cabin.save()
            .then((cabin) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(cabin);
            }, (err) => next(err));
        }
        else {
            err = new Error('Cabin ' + req.params.cabinId + ' not found');
            err.status = 404;
        }
    }, (err) => next(err))
    .catch((err) => next(err));
});

cabinRouter.route('/:cabinId/comments/:commentId')
.options(cors.corsWithOptions, (req,res) => {res.sendStatus(200); })
.get(cors.cors, (req, res, next) => {
    Cabins.findById(req.params.cabinId)
    .populate('comments.author')
    .then((cabin) => {
        if (cabin != null && cabin.comments.id(req.params.commentId) != null) {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json(cabin.comments.id(req.params.commentId));
        }
        else if (cabin == null ) {
            err = new Error('Cabin ' + req.params.cabinId + ' not found');
            err.status = 404;
            return next(err);
        }
        else {
            err = new Error('Comment ' + req.params.commentId + ' not found');
            err.status = 404;
            return next(err);
        }
    }, (err) => next(err))
    .catch((err) => next(err));
})
.post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    res.statusCode = 403;
    res.end('POST operation not supported on /cabins/' + req.params.cabinId + 
    '/comments/' + req.params.commentId);
})
.put(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Cabins.findById(req.params.cabinId)
    .then((cabin) => {
        if (cabin != null && cabin.comments.id(req.params.commentId) != null) {
            if (req.body.rating) {
                cabin.comments.id(req.params.commentId).rating = req.body.rating;
            }
            if (req.body.comment) {
                cabin.comments.id(req.params.commentId).comment = req.body.comment;
            }
            cabin.save()
            .then((cabin) => {
                Cabins.findById(cabin._id)
                .populate('comments.author')
                .then((cabin) => {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(cabin);  
                })              
            }, (err) => next(err));
            
        }
        else if (cabin == null) {
            err = new Error('Cabin ' + req.params.cabinId + ' not found');
            err.status = 404;
            return next(err);
        }
        else {
            err = new Error('Comment ' + req.params.commentId + ' not found');
            err.status = 404;
            return next(err);
        }
    }, (err) => next(err))
    .catch((err) => next(err));
})
.delete(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Cabins.findById(req.params.cabinId)
    .then((cabin) => {
        if (cabin != null && cabin.comments.id(req.params.commentId) != null && req.body.author._id) {
            cabin.comments.id(req.params.commentId).remove();
            cabin.save()
            .then((cabin) => {
                Cabins.findById(cabin._id)
                .populate('comments.author')
                .then((cabin) => {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(cabin);  
                })               
            }, (err) => next(err));
        }
        else if (cabin == null) {
            err = new Error('Cabin ' + req.params.cabinId + ' not found');
            err.status = 404;
            return next(err);
        }
        else {
            err = new Error('Cabin ' + req.params.cabinId + ' not found');
            err.status = 404;
            return next(err);
        }
    }, (err) => next(err))
    .catch((err) => next(err));
});

module.exports = cabinRouter;