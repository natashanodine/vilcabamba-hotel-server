const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const authenticate = require('../authenticate');
const cors = require('./cors');
const Ecoactivities = require('../models/ecoactivities');

const ecoactivityRouter = express.Router();

ecoactivityRouter.use(bodyParser.json());


ecoactivityRouter.route('/')
.options(cors.corsWithOptions, (req,res) => {res.sendStatus(200); })
.get(cors.cors, (req, res, next) => {
    Ecoactivities.find(req.query)
    .populate('comments.author')
    .then((ecoactivity) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(ecoactivity);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.post(cors.corsWithOptions, /*authenticate.verifyUser, authenticate.verifyAdmin,*/ (req, res, next) => {
    Ecoactivities.create(req.body)
    .then((ecoactivity) => {
        console.log('Ecoactivity Created', ecoactivity);
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(ecoactivity);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.put(cors.corsWithOptions, authenticate.verifyUser,authenticate.verifyAdmin, (req, res, next) => {
    res.statusCode = 403;
    res.end('PUT operation not supported on /ecoactivities');
})
.delete(cors.corsWithOptions, /*authenticate.verifyUser, authenticate.verifyAdmin,*/ (req, res, next) => {
    Ecoactivities.remove({})
    .then((resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    }, (err) => next(err))
    .catch((err) => next(err));
});

ecoactivityRouter.route('/:ecoactivityId')
.options(cors.corsWithOptions, (req,res) => {res.sendStatus(200); })
.get(cors.cors, (req, res, next) => {
    Ecoactivities.findById(req.params.ecoactivityId)
    .populate('comments.author')
    .then((ecoactivity) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(ecoactivity);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.post(cors.corsWithOptions,/*authenticate.verifyUser, authenticate.verifyAdmin,*/ (req, res, next) => {
    res.statusCode = 403;
    res.end('POST operation not supported on /ecoactivities/' + req.params.ecoactivityId);
})
.put(cors.corsWithOptions, /*authenticate.verifyUser, authenticate.verifyAdmin,*/ (req, res, next) => {
    Ecoactivities.findByIdAndUpdate(req.params.ecoactivityId, {
        $set: req.body
    }, {new: true})
    .then((ecoactivity) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(ecoactivity);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.delete(cors.corsWithOptions, /*authenticate.verifyUser, authenticate.verifyAdmin,*/ (req, res, next) => {
    Ecoactivities.findByIdAndRemove(req.params.ecoactivityId)
    .then((resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    }, (err) => next(err))
    .catch((err) => next(err));
});

ecoactivityRouter.route('/:ecoactivityId/comments')
.options(cors.corsWithOptions, (req,res) => {res.sendStatus(200); })
.get(cors.cors, (req, res, next) => {
    Ecoactivities.findById(req.params.ecoactivityId)
    .populate('comments.author')
    .then((ecoactivity) => {
        if (ecoactivity != null) {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json(ecoactivity.comments);
        }
        else {
            err = new Error('Ecoactivity ' + req.params.ecoactivityId + ' not found');
            err.status = 404;
            return next(err);
        }
    }, (err) => next(err))
    .catch((err) => next(err));
})
.post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Ecoactivities.findById(req.params.ecoactivityId)
    .then((ecoactivity) => {
        if (ecoactivity != null) {
            req.body.author = req.user._id;
            ecoactivity.comments.push(req.body);
            ecoactivity.save()
            .then((ecoactivity) => {
                Ecoactivities.findById(ecoactivity._id)
                .populate('comments.author')
                .then((ecoactivity) => {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(ecoactivity);
                })            
            }, (err) => next(err));
        }
        else {
            err = new Error('Ecoactivity ' + req.params.ecoactivityId + ' not found');
            err.status = 404;
            return next(err);
        }
    }, (err) => next(err))
    .catch((err) => next(err));
})
.put(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    res.statusCode = 403;
    res.end('PUT operation not supported on /ecoactivities/' + req.params.ecoactivityId + '/comments');
})
.delete(cors.corsWithOptions, authenticate.verifyUser,authenticate.verifyAdmin, (req, res, next) => {
    Ecoactivities.findById(req.params.ecoactivityId)
    .then((ecoactivity) => {
        if (ecoactivity != null || req.body.author._id) {
            for (var i = (ecoactivity.comments.length -1); i >= 0; i--) {
                ecoactivity.comments.id(ecoactivity.comments[i]._id).remove();
            }
            ecoactivity.save()
            .then((ecoactivity) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(ecoactivity);
            }, (err) => next(err));
        }
        else {
            err = new Error('Ecoactivity ' + req.params.ecoactivityId + ' not found');
            err.status = 404;
        }
    }, (err) => next(err))
    .catch((err) => next(err));
});

ecoactivityRouter.route('/:ecoactivityId/comments/:commentId')
.options(cors.corsWithOptions, (req,res) => {res.sendStatus(200); })
.get(cors.cors, (req, res, next) => {
    Ecoactivities.findById(req.params.ecoactivityId)
    .populate('comments.author')
    .then((ecoactivity) => {
        if (ecoactivity != null && ecoactivity.comments.id(req.params.commentId) != null) {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json(ecoactivity.comments.id(req.params.commentId));
        }
        else if (ecoactivity == null ) {
            err = new Error('Ecoactivity ' + req.params.ecoactivityId + ' not found');
            err.status = 404;
            return next(err);
        }
        else {
            err = new Error('Comment ' + req.params.commentId + ' not found');
            err.status = 404;
            return next(err);
        }
    }, (err) => next(err))
    .catch((err) => next(err));
})
.post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    res.statusCode = 403;
    res.end('POST operation not supported on /ecoactivities/' + req.params.ecoactivityId + 
    '/comments/' + req.params.commentId);
})
.put(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Ecoactivities.findById(req.params.ecoactivityId)
    .then((ecoactivity) => {
        if (ecoactivity != null && ecoactivity.comments.id(req.params.commentId) != null) {
            if (req.body.rating) {
                ecoactivity.comments.id(req.params.commentId).rating = req.body.rating;
            }
            if (req.body.comment) {
                ecoactivity.comments.id(req.params.commentId).comment = req.body.comment;
            }
            ecoactivity.save()
            .then((ecoactivity) => {
                Ecoactivities.findById(ecoactivity._id)
                .populate('comments.author')
                .then((ecoactivity) => {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(ecoactivity);  
                })              
            }, (err) => next(err));
            
        }
        else if (ecoactivity == null) {
            err = new Error('Ecoactivity ' + req.params.ecoactivityId + ' not found');
            err.status = 404;
            return next(err);
        }
        else {
            err = new Error('Comment ' + req.params.commentId + ' not found');
            err.status = 404;
            return next(err);
        }
    }, (err) => next(err))
    .catch((err) => next(err));
})
.delete(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Ecoactivities.findById(req.params.ecoactivityId)
    .then((ecoactivity) => {
        if (ecoactivity != null && ecoactivity.comments.id(req.params.commentId) != null && req.body.author._id) {
            ecoactivity.comments.id(req.params.commentId).remove();
            ecoactivity.save()
            .then((ecoactivity) => {
                Ecoactivities.findById(ecoactivity._id)
                .populate('comments.author')
                .then((ecoactivity) => {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(ecoactivity);  
                })               
            }, (err) => next(err));
        }
        else if (ecoactivity == null) {
            err = new Error('Ecoactivity ' + req.params.ecoactivityId + ' not found');
            err.status = 404;
            return next(err);
        }
        else {
            err = new Error('Ecoactivity ' + req.params.ecoactivityId + ' not found');
            err.status = 404;
            return next(err);
        }
    }, (err) => next(err))
    .catch((err) => next(err));
});

module.exports = ecoactivityRouter;