const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const authenticate = require('../authenticate');
const cors = require('./cors');
const Houses = require('../models/houses');

const houseRouter = express.Router();

houseRouter.use(bodyParser.json());


houseRouter.route('/')
.options(cors.corsWithOptions, (req,res) => {res.sendStatus(200); })
.get(cors.cors, (req, res, next) => {
    Houses.find(req.query)
    .populate('comments.author')
    .then((house) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(house);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.post(cors.corsWithOptions, /*authenticate.verifyUser, authenticate.verifyAdmin,*/ (req, res, next) => {
    Houses.create(req.body)
    .then((house) => {
        console.log('House Created', house);
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(house);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.put(cors.corsWithOptions, authenticate.verifyUser,authenticate.verifyAdmin, (req, res, next) => {
    res.statusCode = 403;
    res.end('PUT operation not supported on /houses');
})
.delete(cors.corsWithOptions, /*authenticate.verifyUser, authenticate.verifyAdmin,*/ (req, res, next) => {
    Houses.remove({})
    .then((resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    }, (err) => next(err))
    .catch((err) => next(err));
});

houseRouter.route('/:houseId')
.options(cors.corsWithOptions, (req,res) => {res.sendStatus(200); })
.get(cors.cors, (req, res, next) => {
    Houses.findById(req.params.houseId)
    .populate('comments.author')
    .then((house) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(house);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.post(cors.corsWithOptions,/*authenticate.verifyUser, authenticate.verifyAdmin,*/ (req, res, next) => {
    res.statusCode = 403;
    res.end('POST operation not supported on /houses/' + req.params.houseId);
})
.put(cors.corsWithOptions, /*authenticate.verifyUser, authenticate.verifyAdmin,*/ (req, res, next) => {
    Houses.findByIdAndUpdate(req.params.houseId, {
        $set: req.body
    }, {new: true})
    .then((house) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(house);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.delete(cors.corsWithOptions, /*authenticate.verifyUser, authenticate.verifyAdmin,*/ (req, res, next) => {
    Houses.findByIdAndRemove(req.params.houseId)
    .then((resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    }, (err) => next(err))
    .catch((err) => next(err));
});

houseRouter.route('/:houseId/comments')
.options(cors.corsWithOptions, (req,res) => {res.sendStatus(200); })
.get(cors.cors, (req, res, next) => {
    Houses.findById(req.params.houseId)
    .populate('comments.author')
    .then((house) => {
        if (house != null) {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json(house.comments);
        }
        else {
            err = new Error('House ' + req.params.houseId + ' not found');
            err.status = 404;
            return next(err);
        }
    }, (err) => next(err))
    .catch((err) => next(err));
})
.post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Houses.findById(req.params.houseId)
    .then((house) => {
        if (house != null) {
            req.body.author = req.user._id;
            house.comments.push(req.body);
            house.save()
            .then((house) => {
                Houses.findById(house._id)
                .populate('comments.author')
                .then((house) => {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(house);
                })            
            }, (err) => next(err));
        }
        else {
            err = new Error('House ' + req.params.houseId + ' not found');
            err.status = 404;
            return next(err);
        }
    }, (err) => next(err))
    .catch((err) => next(err));
})
.put(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    res.statusCode = 403;
    res.end('PUT operation not supported on /houses/' + req.params.houseId + '/comments');
})
.delete(cors.corsWithOptions, authenticate.verifyUser,authenticate.verifyAdmin, (req, res, next) => {
    Houses.findById(req.params.houseId)
    .then((house) => {
        if (house != null || req.body.author._id) {
            for (var i = (house.comments.length -1); i >= 0; i--) {
                house.comments.id(house.comments[i]._id).remove();
            }
            house.save()
            .then((house) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(house);
            }, (err) => next(err));
        }
        else {
            err = new Error('House ' + req.params.houseId + ' not found');
            err.status = 404;
        }
    }, (err) => next(err))
    .catch((err) => next(err));
});

houseRouter.route('/:houseId/comments/:commentId')
.options(cors.corsWithOptions, (req,res) => {res.sendStatus(200); })
.get(cors.cors, (req, res, next) => {
    Houses.findById(req.params.houseId)
    .populate('comments.author')
    .then((house) => {
        if (house != null && house.comments.id(req.params.commentId) != null) {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json(house.comments.id(req.params.commentId));
        }
        else if (house == null ) {
            err = new Error('House ' + req.params.houseId + ' not found');
            err.status = 404;
            return next(err);
        }
        else {
            err = new Error('Comment ' + req.params.commentId + ' not found');
            err.status = 404;
            return next(err);
        }
    }, (err) => next(err))
    .catch((err) => next(err));
})
.post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    res.statusCode = 403;
    res.end('POST operation not supported on /houses/' + req.params.houseId + 
    '/comments/' + req.params.commentId);
})
.put(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Houses.findById(req.params.houseId)
    .then((house) => {
        if (house != null && house.comments.id(req.params.commentId) != null) {
            if (req.body.rating) {
                house.comments.id(req.params.commentId).rating = req.body.rating;
            }
            if (req.body.comment) {
                house.comments.id(req.params.commentId).comment = req.body.comment;
            }
            house.save()
            .then((house) => {
                Houses.findById(house._id)
                .populate('comments.author')
                .then((house) => {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(house);  
                })              
            }, (err) => next(err));
            
        }
        else if (house == null) {
            err = new Error('House ' + req.params.houseId + ' not found');
            err.status = 404;
            return next(err);
        }
        else {
            err = new Error('Comment ' + req.params.commentId + ' not found');
            err.status = 404;
            return next(err);
        }
    }, (err) => next(err))
    .catch((err) => next(err));
})
.delete(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Houses.findById(req.params.houseId)
    .then((house) => {
        if (house != null && house.comments.id(req.params.commentId) != null && req.body.author._id) {
            house.comments.id(req.params.commentId).remove();
            house.save()
            .then((house) => {
                Houses.findById(house._id)
                .populate('comments.author')
                .then((house) => {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(house);  
                })               
            }, (err) => next(err));
        }
        else if (house == null) {
            err = new Error('House ' + req.params.houseId + ' not found');
            err.status = 404;
            return next(err);
        }
        else {
            err = new Error('House ' + req.params.houseId + ' not found');
            err.status = 404;
            return next(err);
        }
    }, (err) => next(err))
    .catch((err) => next(err));
});

module.exports = houseRouter;